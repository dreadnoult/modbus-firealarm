﻿Imports System
Imports System.Data.OleDb
Imports System.IO
Imports FieldTalk
Imports FieldTalk.BusProtocolErrors

Module MBus_Part
    Class MyDatatable
        Inherits MbusDataTableInterface

        Public localRegisters(5000) As Short


        Protected Overrides Function readHoldingRegistersTable(ByVal startRef As Int32, ByVal regArr() As Int16) As Boolean
            Dim i As Integer

            ' Console.WriteLine("readHoldingRegisters from " & startRef & ", " & regArr.Length & " references")
            ' Adjust Modbus reference counting from 1-based to 0-based
            'startRef = startRef - 1
            ' Validate range
            If startRef + regArr.Length > localRegisters.Length Then
                Return False
            End If
            ' Copy registers from local data array to Modbus
            For i = 0 To regArr.Length - 1
                regArr(i) = localRegisters(startRef + i)
            Next
            Return True

        End Function


        Protected Overrides Function writeHoldingRegistersTable(ByVal startRef As Int32, ByVal regArr() As Int16) As Boolean
            Dim i As Integer
            Console.WriteLine("writeHoldingRegisters from " & startRef & ", " & regArr.Length & " references")
            ' Adjust Modbus reference counting from 1-based to 0-based
            startRef = startRef - 1
            ' Validate range
            If startRef + regArr.Length > localRegisters.Length Then
                Return False
            End If
            ' Copy registers from Modbus to local data block
            For i = 0 To regArr.Length - 1
                localRegisters(startRef + i) = regArr(i)
            Next
            Return True
        End Function

    End Class


    '''
    ''' Slave thread
    '''
    Class TcpSlaveApp

        Private mbusServer As MbusTcpSlaveProtocol = New MbusTcpSlaveProtocol
        Public dataTable As MyDatatable = New MyDatatable

        '''
        ''' Starts up server
        '''
        Public Sub startupServer()
            Dim result As Integer

            result = mbusServer.addDataTable(1, dataTable) ' Unit ID is 1
            If result = FTALK_SUCCESS Then
                result = mbusServer.startupServer()
            End If
            If result <> FTALK_SUCCESS Then
                Console.WriteLine(getBusProtocolErrorText(result))
                Environment.Exit(result)
            End If
            Console.WriteLine("Modbus server started on TCP interface.")
        End Sub


        '''
        ''' Shutdown server
        '''
        Public Sub shutdownServer()
            mbusServer.shutdownServer()
        End Sub


        '''
        ''' Run server
        '''
        Public Sub runServer()
            Dim result As Integer
            'Do
            'dataTable.localRegisters = Data_Mas
            result = mbusServer.serverLoop()
            'Loop Until result <> FTALK_SUCCESS
            'If result <> FTALK_SUCCESS Then
            '    Console.WriteLine(getBusProtocolErrorText(result))
            '    Console.ReadLine()
            'End If
        End Sub

    End Class
End Module
