﻿Imports System
Imports System.IO
Imports System.Data.OleDb

Module DB_Access

    Const MPB_Base_Addr As Integer = 0

    Function Find_Index_MPB(ByRef Base_mpb As Integer, ByRef Node As Integer, ByRef Shleif As Integer, ByRef Address As Integer, ByRef Type As String) As Integer
        Dim Current_Address As Integer
        Dim Base As Integer

        Select Case Node
            Case 1 'MPB
                Base = 0
                If Shleif < 8 Then
                    If Type = "Detector" Then
                        Current_Address = Base + (Shleif * 200) - 100
                    ElseIf Type = "Module" Then
                        Current_Address = Base + (Shleif * 200)
                    End If
                    Current_Address = Current_Address + Address
                Else
                    Current_Address = 5001
                End If


            Case 2 'Parking
                Base = 3400
                If Type = "Detector" Then
                    Current_Address = Base + (Shleif * 200) - 100
                    Current_Address = Current_Address + Address
                ElseIf Type = "Module" Then
                    Current_Address = Base + (Shleif * 200)
                    Select Case Address
                        Case 1
                            Current_Address = Current_Address + 1
                        Case 2
                            Current_Address = Current_Address + 2
                        Case 3
                            Current_Address = Current_Address + 3
                        Case 4
                            Current_Address = Current_Address + 15
                        Case 5
                            Current_Address = Current_Address + 4
                        Case 6
                            Current_Address = Current_Address + 5
                        Case 7
                            Current_Address = Current_Address + 6
                        Case 8
                            Current_Address = Current_Address + 16
                        Case 9, 10, 11, 12, 13, 14
                            Current_Address = Current_Address + Address - 2

                    End Select
                End If

            Case 3 'OSR
                Base = 4600
                If Type = "Detector" Then
                    If Address < 38 Then
                        Current_Address = Base + (Shleif * 200) - 100
                    ElseIf ((Address > 38) And (Address < 43)) Then
                        Current_Address = Base + (Shleif * 200) - 101
                    Else
                        Current_Address = Base + (Shleif * 200) - 102
                    End If
                ElseIf Type = "Module" Then
                    Current_Address = Base + (Shleif * 200)
                End If
                Current_Address = Current_Address + Address
            Case 4 'Gatehouse
                Base = 3600
                If Type = "Detector" Then
                    Current_Address = Base + (Shleif * 200) - 100
                ElseIf Type = "Module" Then
                    Current_Address = Base + (Shleif * 200)
                End If
                Current_Address = Current_Address + Address
            Case 5 'Contractors workshop
                Base = 3800
                If Type = "Detector" Then
                    Current_Address = Base + (Shleif * 200) - 100
                ElseIf Type = "Module" Then
                    Current_Address = Base + (Shleif * 200)
                End If
                Current_Address = Current_Address + Address
            Case 6 'Maintenance shop
                Base = 4200
                If Type = "Detector" Then
                    Current_Address = Base + (Shleif * 200) - 100
                    If Shleif = 1 Then 'Modbus list error fix. The addresses in  4321
                        Current_Address = Current_Address + 19
                    End If
                ElseIf Type = "Module" Then
                    Current_Address = Base + (Shleif * 200)
                End If
                Current_Address = Current_Address + Address
            Case 7 'Incinerator
                Base = 3200
                If Type = "Detector" Then
                    Current_Address = Base + (Shleif * 200) - 100
                ElseIf Type = "Module" Then
                    Current_Address = Base + (Shleif * 200)
                End If
                If Address = 17 Then
                    Current_Address = Current_Address + 12
                ElseIf Address = 18 Then
                    Current_Address = Current_Address + 13
                Else
                    Current_Address = Current_Address + Address
                End If

        End Select
        Return Current_Address
    End Function

    Function Get_State(ByRef Message As String, ByRef Type As String)
        Dim State As Integer

        If Message.Contains("All Systems Normal") Then
            State = 0
        ElseIf Message.Contains("Alarm Detector") Then
            State = 1
        ElseIf Message.Contains("Invalid Reply") Then
            State = 9
        ElseIf Message.Contains("Point Disabled") Then
            State = 61
        ElseIf Message.Contains("Point Enabled") Then
            State = 62
        ElseIf Message.Contains("ouput activated") Then
            State = 70
        ElseIf Message.Contains("output Reset") Then
            State = 71
        ElseIf Message.Contains("Alarm Module") Then
            State = 81
        Else
            State = 101
        End If
        Return State

    End Function

    Sub DB_Acc(ByRef Data_Mas() As Short, ByRef Data_Save() As Short)

        Dim TCPServer As TcpSlaveApp = New TcpSlaveApp

        Dim StrConnect As String = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=C:\NET2010N\Eventi.mdb" 'Connection string. Filepath can be adjusted
        Dim Querry As String = "SELECT * FROM LogEventi WHERE(((LogEventi.ID1) = 17459))"

        Dim Connection As New OleDbConnection(StrConnect) 'Connection
        Dim DataReader, DataCheck As OleDbDataReader 'DataReader to read rows, DataCheck to check if there are absent IDs in the table
        Dim Cmd, CmdCheck As OleDbCommand 'Command
        Dim Err_Check, Msg_Bool, Flag As Boolean

        Dim Curr_Linea, Curr_Nodo, Curr_Lib, Curr_Address, index, Curr_State, i, a As Integer
        Dim Curr_Type, Curr_Msg, EventType, State As String
        Dim Curr_ID As Long
        'Dim Data_Mas(5000), Data_Save(5000) As Short

        Dim myWriteFile As New StreamWriter("C:\logs.txt")

        TCPServer.startupServer()
        Connection.Open()
        Flag = False

        'Get the first ID
        Cmd = New OleDbCommand("SELECT Min(LogEventi.ID1) FROM LogEventi", Connection)
        DataReader = Cmd.ExecuteReader()
        DataReader.Read()
        Curr_ID = DataReader(0)
        DataReader.Close()
        i = 0
        Console.WriteLine(Curr_ID)

        'Main loop. During normal operation the application should never go out of it.
        While Err_Check = False

            If Flag = True Then
                TCPServer.dataTable.localRegisters = Data_Mas
                TCPServer.runServer()
            End If
            
            Try
                Querry = "SELECT * FROM LogEventi WHERE(((LogEventi.ID1) = " & Curr_ID & "))"
                Cmd = New OleDbCommand(Querry, Connection)
                DataReader = Cmd.ExecuteReader()
                DataReader.Read()

                If (DataReader.HasRows) Then
                    Msg_Bool = False

                    Curr_Type = DataReader(7).ToString
                    Curr_Msg = DataReader(8).ToString
                    Curr_State = Get_State(Curr_Msg, Curr_Type) 'Check the current message status

                    If Curr_State = 0 Then 'If ALL SYSTEMS NORMAL
                        For a = 0 To 5000
                            If (Data_Mas(a) <> 61) Then 'Set 0 to all nodes, apart from disabled (Status code 61)
                                If Data_Mas(a) = 71 Then
                                    Data_Mas(a) = 70
                                Else
                                    Data_Mas(a) = 0
                                    Data_Save(a) = 0
                                End If
                            End If
                        Next
                        Console.WriteLine("ID: " & Curr_ID & " ALL SYSTEMS NORMAL")
                        myWriteFile.WriteLine("ID: " & Curr_ID & " ALL SYSTEMS NORMAL")
                    End If

                    If ((DataReader(3) <> "") And (DataReader(4) <> "") And (DataReader(5) <> "") And (DataReader(6) <> "")) Then 'If all the address fields are presented
                        'Console.WriteLine(Curr_ID) 'Вывод
                        EventType = DataReader(2).ToString
                        Curr_Linea = DataReader(3)
                        Curr_Nodo = DataReader(4)
                        Curr_Lib = DataReader(5)
                        Curr_Address = DataReader(6)

                        If Curr_State <> 101 Then 'If this is a status message
                            index = Find_Index_MPB(0, Curr_Nodo, Curr_Lib, Curr_Address, Curr_Type)
                            If index <> 5001 Then
                                If Curr_State = 61 Then
                                    If Data_Mas(index) <> 61 Then
                                        Data_Save(index) = Data_Mas(index)
                                        Data_Mas(index) = Curr_State
                                        Console.WriteLine("Value " & Data_Save(index) & " Saved")
                                        myWriteFile.WriteLine("Value " & Data_Save(index) & " Saved")
                                    Else
                                        Console.WriteLine("Value " & Data_Save(index) & " Saved")
                                        myWriteFile.WriteLine("Value " & Data_Save(index) & " Saved")
                                    End If
                                ElseIf Curr_State = 62 Then
                                    Data_Mas(index) = Data_Save(index)
                                    Console.WriteLine("Value " & Data_Mas(index) & " Loaded")
                                    myWriteFile.WriteLine("Value " & Data_Mas(index) & " Loaded")
                                Else
                                    Data_Mas(index) = Curr_State
                                End If
                                i = i + 1
                                myWriteFile.WriteLine("ID: " & Curr_ID & " Index: " & index & " Value: " & Curr_State & " " & i)
                                Console.WriteLine("ID: " & Curr_ID & " Index: " & index & " Value: " & Curr_State)
                            End If
                        End If
                    End If
                    Curr_ID = Curr_ID + 1
                Else
                    CmdCheck = New OleDbCommand("SELECT LogEventi.ID1 FROM(LogEventi) WHERE (((LogEventi.ID1)>" & Curr_ID & "))", Connection)
                    DataCheck = CmdCheck.ExecuteReader
                    DataCheck.Read()
                    If (DataCheck.HasRows) Then
                        Curr_ID = Curr_ID + 1
                        Console.WriteLine(Curr_ID)
                    Else
                        If Msg_Bool = False Then
                            Console.WriteLine("Waiting Event " & i & " ID: " & Curr_ID)
                            Msg_Bool = True
                            Flag = True
                        End If
                    End If
                    DataCheck.Close()
                End If
                DataReader.Close()

            Catch ex As Exception
                TCPServer.shutdownServer()
                Err_Check = True
                State = ex.ToString
                Console.WriteLine(ex.Message)
                Console.WriteLine(State)
                Console.WriteLine(i)
            End Try
        End While

        Console.ReadLine()


    End Sub
End Module
