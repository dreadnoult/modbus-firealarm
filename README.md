# Notinet-ModbusTCP

The program deals with both **Notifier Notinet 2000(2010)** and **Notinet 2002(2010N)** and is intended to transmit messages coming from Notifier Fire Alarm system to **ModbusTCP** network.
>**Note:** Only the messages registered by the Notifier Notinet program are to be decoded and transmitted.

## Prerequisites
Built with Visual Studio 2017 Community Edition using the following components:

 - [Microsoft .Net Framework 2.5](https://www.microsoft.com/de-de/download/details.aspx?id=21) or above
 - [FieldTalk Modbus Slave Library for .NET 2.5.1](http://www.modbusdriver.com/shop/product_info.php?products_id=71) 

>**Note:** Please be aware that according to [licensing limitations ](https://www.visualstudio.com/vs/compare/) Visual Studio Community Edition cannot be used by Enterprise organisations, i.e. organisations that has >5 users, >250 PCs or > $1 Million US Dollars in annual revenue.

### Notifier Notinet

The program uses **Microsoft.Jet.OLEDB.4.0** Provider in order to access the database stored in *&lt;Notifier Notinet root folder&gt;/Eventi.mdb>*. 
> **Note:** The provider is usualy included in Notifier Notiner installation package.

## Installing

The program can be downloaded from the repository as a zip-archive or cloned using [git tool](https://git-scm.com/):
```
git clone git@gitlab.com:dreadnoult/modbus-firealarm.git
```
 The repository contains two branches, `Master` and `Dev`. The main differences between them is in the way the path to the Notifier Notinet database file is specified. In `Master` it can be specified during the building phase so that it cannot be changed further. For the version in `Dev`, the filepath can be specified while running the program using the program start arguments as a parameter.

 1. Depending of the version of program used (`Master` or `Dev`) there must be additional steps taken before the actual installation. 
	 - If the version `Master` is used, please specify the path to the file *&lt;Notifier Notinet root folder&gt;/Eventi.mdb>* in the module *DB_Access.vb* within the project before building. 
	 - In case the `Dev` version is used this step isn't necessary.
 2. Build the entire solution using the *Build -> Build Solution* menu for the ***Release*** configuration.
 3. Copy the following files from *&lt;Project Folder&gt;/bin/**Release**>* to any folder on a PC with Notifier Notinet:
	 - *MBus_FA.exe*
	 - *mbusslave.net.dll*
 4. If the version `Dev` is used, create a shortcut to the copied file *MBus_FA.exe*, right click on the shortcut,  add the path to the database file in quotes "" to the end of the *Target* field. For example, the full target field might look like `C:\MBus_FA25\MBus_FA.exe "C:\NOTINET2010\Eventi.mdb"`

## Usage

Run the program by the double click on the executable itself (`Master` version) or created shortcut (`Dev` version). The program analyses the current state of the devices connected to the Notifier Fire Alarm system and starts the ModbusTCP server (slave) as soon as the devices' state is read and decoded. The process of updating of the values may take some time, especially when the FA system is beeing run for a long time.
The program behaviour can be analaysed accessing the log file, by default stored in *C:/Logs.txt*

### Modbus

#### Adresses
The program acts as a MosbusTCP slave providing a Holding Register field for each of the devices connected to the Notifier Fire Alarm system. Any device's Modbus register address can be defined using the formula:

>device_Modbus_Address = Base_Address + (Line_Number * 200) - 100 + Point_Address for Detectors (Sensors)

>device_Modbus_Address = Base_Address + (Line_Number * 200) + Point_Address for Modules (Actuators)

where *Line_Number* and *Point_Address* correspond to the values configured in Notifier Fire Alarm system, and *Base_Address* depends on building, where the device is installed. 
The base addresses for different buildings are defined as follows:

| Building        | Base address |           
| ------------- |:-------------:|
| Multi-Purpouse Building      | 40000 | 
| Incinerator     | 43200     |  
| Gatehouse | 43600      |  
| Contractor's maintenance workshop | 43800      |  
| Maintenance shop | 44219      |  
| OSR storage | 44600      |  

#### Example
1. A W2-YSH03300-829 smoke detector installed at the Contractor's maintenance workshop (*Base_Address* = 43800) has Notifier Fire Alarm address L01D09, or in other words *Line_Number* = 1 and *Point_Address* = 9. Modbus register address for the sensor is 43909.
2. A W2-MCP03300-91 pull station installed at the Gatehouse (*Base_Address* = 43600) has Notifier Fire Alarm address L01M05, or in other words *Line_Number* = 1 and *Point_Address* = 5. Modbus register address for the station is 43805.

#### Status codes
Each Modbus register represents current state of a device by means of a status code.

| Code        | Status          
| ------------- |-------------|
| 00      | Normal Condition (non-alarm) | 
| 01     | Alarm Condition/ NONA Non-Alarm Condition     |  
| 08 | Gas Detector Fault      |  
| 09 | Invalid Reply     |  
| 11 | Point Trouble      |  
| 12 | Trouble Monitor      | 
| 14 | Incorrect Device Type ID      |  
| 57 | No Device Mapped (unused)      |  
| 61 | Device Disabled      |  
| 70 | Output Point ON (Active Relay / Control Module)      | 
| 71 | Output Point OFF (Non-Active Relay / Control Module)      |
| 81 | Status Monitor Module Alarm (Type ID = STAT)      |


